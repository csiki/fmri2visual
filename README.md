Predict the seen images out of fMRI BOLD images by running a recurrent neural network.

Download datasets from [https://crcns.org/data-sets/vc/vim-2/about-vim-2](https://crcns.org/data-sets/vc/vim-2/about-vim-2),
unpack them in the root folder of this repository,
install `tensorflow`, `numpy` and `tables` Python packages
and you can start running the `decoding_model.py` script immediately!

Train, test, then draw images to test how accurately the model represents the seen images using only corresponding fMRI BOLD images of the visual pathway.

![V1, V2, V3, ... are inputs to the network, fMRI BOLD data of brain regions along the visual pathway. l1, l2, l3, ... are densely connected layers; notice how the last layer feeds back to the first, which allows the information of previous frames (neural activations in the occipital lobe) to be passed on when determining the next frames. The sequence of inputs are given in order to vaguely simulate how the human visual cortex process the information of seen images.](https://i.imgur.com/DWxGj59.png)
V1, V2, V3, ... are inputs to the network, fMRI BOLD data of brain regions along the visual pathway. l1, l2, l3, ... are densely connected layers; notice how the last layer feeds back to the first, which allows the information of previous frames (neural activations in the occipital lobe) to be passed on when determining the next frames. The sequence of inputs are given in order to vaguely simulate how the human visual cortex process the information of seen images.

Huth AG, Lee T, Nishimoto S, Bilenko NY, Vu AT and Gallant JL (2016) Decoding the Semantic Content of Natural Movies from Human Brain Activity. Front. Syst. Neurosci. 10:81. doi: 10.3389/fnsys.2016.00081