import os
from time import sleep
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv2DTranspose, Dense, Input, Concatenate, UpSampling2D, BatchNormalization, Reshape, LeakyReLU, Activation, Lambda
from tensorflow.keras.utils import plot_model
from work_dat_voxel import load_all, rand_gen_data
import matplotlib.pyplot as plt


# TODO include more features, building the brain model and connections
# TODO try to generalize between subjects


class DecodingModel:

    def __init__(self, stim_file, input_sizes, layer_sizes, run_len, batch_size):
        self.resp = self.stim = self.max_nan_len = self.response_sizes = None

        self.input_sizes = input_sizes
        self.batch_size = batch_size
        self.run_len = run_len  # how many seconds the recursive network should go through
        self.layer_sizes = layer_sizes
        self.stim_file = stim_file

        # visual pathway
        self.inputs = []
        self.outputs = []
        self.dense_layers = []

        # create dense layers
        for in_size, l_size in zip(self.input_sizes, self.layer_sizes):
            self.dense_layers.append(Dense(l_size, activation='relu'))

        # prepare conv layers
        self.conv_layers = [Conv2DTranspose(filters=32, kernel_size=5, padding='same'),
                            Conv2DTranspose(filters=16, kernel_size=3, padding='same'),
                            Conv2DTranspose(filters=8, kernel_size=3, padding='same'),
                            Conv2DTranspose(filters=1, kernel_size=1, padding='same')]

        # create unfolded recurrent network
        side_input = Input(shape=(self.layer_sizes[-1],), batch_size=batch_size)
        self.inputs.append(side_input)
        for r in range(self.run_len):
            l = 0
            for in_size, l_size in zip(self.input_sizes, self.layer_sizes):
                inp = Input(shape=(in_size,), batch_size=batch_size)
                self.inputs.append(inp)
                conc = Concatenate(axis=1)([side_input, inp])
                side_input = self.dense_layers[l](conc)
                l += 1

            # deconvolution
            x = Reshape((16, 16, 1))(side_input)
            x = UpSampling2D((2, 2))(x)  # 16x16
            x = LeakyReLU()(self.conv_layers[0](x))
            x = BatchNormalization()(x)

            x = UpSampling2D((2, 2))(x)  # 32x32
            x = LeakyReLU()(self.conv_layers[1](x))
            x = BatchNormalization()(x)

            x = UpSampling2D((2, 2))(x)  # 64x64
            x = LeakyReLU()(self.conv_layers[2](x))
            x = BatchNormalization()(x)

            # x = UpSampling2D((2, 2))(x)  # 128x128
            x = LeakyReLU()(self.conv_layers[3](x))

            x = BatchNormalization()(x)

            x = Reshape((128, 128))(x)
            x = Activation('tanh')(x)
            self.outputs.append(x)

        self.model = Model(inputs=self.inputs, outputs=self.outputs)
        self.model.summary()
        # plot_model(self.model)
        self.model.compile(optimizer='adam', loss='mse', loss_weights=list(np.linspace(0.5, 1, self.run_len)))
        print('MODEL COMPILED')

    def train(self, nepoch, ntimes, model_path, Tt, subject_i=0, save_after=50):
        # load data
        if self.resp is None:
            self.resp, self.stim, self.max_nan_len, self.response_sizes = load_all(self.stim_file)
        print('DATA LOADED')

        # load model if exists
        if os.path.isfile(model_path):
            self.model = tf.keras.models.load_model(model_path)
            print('PREVIOUS MODEL LOADED')

        loss = 0
        for e in range(nepoch):
            x, y = rand_gen_data(self.resp[subject_i]['train'], self.stim['train'], self.run_len, Tt,
                                 self.max_nan_len[subject_i]['train'], self.batch_size, self.response_sizes[subject_i])

            initial_side_input = np.zeros((self.batch_size, self.layer_sizes[-1]), dtype=np.float32)
            for t in range(ntimes):
                loss = self.model.train_on_batch([initial_side_input] + x, y)

            print('.', end='')
            if e % 10 == 0:
                print('EPOCH {}: LOSS: '.format(e) + str(loss))

            if e % save_after == 0:
                self.model.save(model_path)
                print('MODEL SAVED')

    def test(self, ntries, model_path, Tv, subject_i=0):
        # load data
        if self.resp is None:
            self.resp, self.stim, self.max_nan_len, self.response_sizes = load_all(self.stim_file)
        print('DATA LOADED')

        # load model if exists
        if os.path.isfile(model_path):
            self.model = tf.keras.models.load_model(model_path)
            print('PREVIOUS MODEL LOADED')
        else:
            print('NO MODEL FOUND')
            return -1

        overall_loss = 0
        initial_side_input = np.zeros((self.batch_size, self.layer_sizes[-1]), dtype=np.float32)
        for t in range(ntries):
            x, y = rand_gen_data(self.resp[subject_i]['validation'], self.stim['validation'], self.run_len, Tv,
                                 self.max_nan_len[subject_i]['validation'], self.batch_size, self.response_sizes[subject_i])
            loss = self.model.evaluate([initial_side_input] + x, y, batch_size=self.batch_size)
            overall_loss += np.sum(loss)
        overall_loss = overall_loss / ntries

        print('OVERALL LOSS: {}'.format(overall_loss))
        return overall_loss

    def test_imgs(self, ntries, model_path, T, train_or_valid='validation', subject_i=0):
        # load data
        if self.resp is None:
            self.resp, self.stim, self.max_nan_len, self.response_sizes = load_all(self.stim_file)
        print('DATA LOADED')

        # load model if exists
        if os.path.isfile(model_path):
            self.model = tf.keras.models.load_model(model_path)
            print('PREVIOUS MODEL LOADED')
        else:
            print('NO MODEL FOUND')
            return -1

        fig, axarr = plt.subplots(1, 2)
        img_real = axarr[0].imshow(np.zeros((128, 128)), vmin=0, vmax=1, cmap=plt.get_cmap('gray'))
        img_pred = axarr[1].imshow(np.zeros((128, 128)), vmin=0, vmax=1, cmap=plt.get_cmap('gray'))
        axarr[0].axis('off')
        axarr[1].axis('off')
        # plt.show(block=False)
        # sleep(5)

        initial_side_input = np.zeros((self.batch_size, self.layer_sizes[-1]), dtype=np.float32)
        for t in range(ntries):
            x, y = rand_gen_data(self.resp[subject_i][train_or_valid], self.stim[train_or_valid], self.run_len, T,
                                 self.max_nan_len[subject_i][train_or_valid], self.batch_size, self.response_sizes[subject_i])

            pred_outputs = self.model.predict([initial_side_input] + x, batch_size=self.batch_size)

            # for out_i in range(pred_outputs.shape[0]):
            #     img_real.set_data(y[out_i])
            #     img_pred.set_data(pred_outputs[out_i])
            #     plt.draw()
            #     sleep(0.7)

            for batch in range(self.batch_size):
                for run in range(self.run_len):
                    img_real.set_data(y[run][batch, :, :])  # (run_len, batch_size, 128, 128)
                    img_pred.set_data(pred_outputs[run][batch, :, :])
                    plt.draw()
                    fig.savefig('img/{}_{}.png'.format(batch, run))
                    # sleep(1)


if __name__ == '__main__':

    # # tiny test
    # run_len = 2
    # batch_size = 1
    # Tt = 100
    # Tv = 100
    # input_sizes = [20, 20, 20, 20]
    # model = DecodingModel(input_sizes, run_len, batch_size, Tt, Tv)

    # let's go
    run_len = 8
    batch_size = 32
    Tt = 7200  # training
    Tv = 540   # validation
    layer_sizes = [64, 128, 128, 256]
    resp_sizes = {'v1rh': 514, 'v2rh': 781, 'v3rh': 562, 'v4rh': 426, 'v1lh': 494, 'v2lh': 726, 'v3lh': 598, 'v4lh': 308}
    input_sizes = [resp_sizes['v1lh'] + resp_sizes['v1rh'], resp_sizes['v2lh'] + resp_sizes['v2rh'],
                   resp_sizes['v3lh'] + resp_sizes['v3rh'], resp_sizes['v4lh'] + resp_sizes['v4rh']]
    stim_file = 'Stimuli.mat'  # StimuliCORF.mat

    model = DecodingModel(stim_file, input_sizes, layer_sizes, run_len, batch_size)
    model.train(3000, 5, 'decoding_model_256_mse.hdf5', Tt)
    model.test(50, 'decoding_model_256_mse.hdf5', Tv)
    model.test_imgs(20, 'decoding_model_256_mse.hdf5', Tt, 'train')
