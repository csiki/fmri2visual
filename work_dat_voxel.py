import tables
import numpy as np


def load_file(fname):
    f = tables.open_file(fname)
    return f


def load_resp_data(data, tt, roi):  # tt is either rt or rv
    r = data.get_node('/' + tt)[:]
    rroi = data.get_node('/roi/' + roi)[:].flatten()
    idx = np.nonzero(rroi == 1)[0]
    # idx = np.random.randint(0, len(r), (10,))  # for testing
    roid = r[idx]
    isnan = np.isnan(roid).any(axis=1)
    nnan_row = np.sum(isnan)
    return nnan_row, np.transpose(roid)


def load_stim_data(data, tt, T, fps, gray_scale=False, CORF=True):  # tt is either st or sv
    snode = data.get_node('/' + tt)  # (T*fps, channel, x, y)
    if CORF:
        s = snode[0::fps, 0, :, :]  # get first image in each second only, and the first channel (CORF)
    else:
        if gray_scale:
            s = np.mean(snode[0::fps, :, :, :], axis=1)
        else:
            s = snode[0::fps, :, :, :]

    s = s.astype(np.float32)
    if not CORF:
        s /= 255.
    return s


def rand_gen_data(r, s, run_len, T, nan_cutoff, batch_size, resp_sizes):
    # r here is data already selected for subject and training/validation, same for nan_cutoff (scalar) and resp_sizes
    # r[roi] has a dimension of (T-nnan, K), where K is the number of voxels
    # s has the dimension (T, 128, 128)
    # rand the first index of the sequence [0, T-run_len)
    idxs = np.random.randint(nan_cutoff, T - run_len, (batch_size,))
    # cut r and s
    v_sizes = [resp_sizes['v1lh'] + resp_sizes['v1rh'], resp_sizes['v2lh'] + resp_sizes['v2rh'],
               resp_sizes['v3lh'] + resp_sizes['v3rh'], resp_sizes['v4lh'] + resp_sizes['v4rh']]
    # v_sizes = [20, 20, 20, 20]  # for testing
    concat_r = [np.zeros((batch_size, run_len, v_size)) for v_size in v_sizes]  # v1, v2, v3, v4 in order
    for batch_i, idx in enumerate(idxs):  # loop through the batch
        # cut
        cut_r = {}
        for roi, v in r.items():
            tmp = v[1]
            # fff=sss[idx:idx + run_len]
            cut_r[roi] = tmp[idx:idx + run_len]
        # concat lr hemispheres
        for vi in range(1, 5):
            k = 'v' + str(vi)
            concat_r[vi-1][batch_i] = np.concatenate([kv for kr, kv in cut_r.items() if k in kr], axis=1)

    # transpose concat_r to get the form (run_len, batch_size, v_size)
    for vi, v in enumerate(concat_r):
        concat_r[vi] = np.transpose(v, [1, 0, 2])

    # produce the input array for the learning model as: run_len * 4 arrays of size batch_size*v_size
    gen_dataset = []
    for run_i in range(run_len):
        for vi in range(4):
            gen_dataset.append(concat_r[vi][run_i])

    # produce the desired output
    # first cut
    cut_s = np.zeros((batch_size, run_len, 128, 128))
    for batch_i, idx in enumerate(idxs):  # loop through the batch
        cut_s[batch_i] = s[idx:idx+run_len]

    # transpose to (run_len, batch_size, 128, 128), then separate arrays
    cut_s = np.transpose(cut_s, [1, 0, 2, 3])
    desired_out = []
    for run_i in range(run_len):
        desired_out.append(cut_s[run_i])

    return gen_dataset, desired_out


def load_all(stim_fname, resp_fnames=['VoxelResponses_subject1.mat']):
    # files_to_load = ['VoxelResponses_subject1.mat', 'VoxelResponses_subject2.mat', 'VoxelResponses_subject3.mat']
    rois_to_load = ['v1rh', 'v2rh', 'v3rh', 'v4rh', 'v1lh', 'v2lh', 'v3lh', 'v4lh']
    fps = 15
    Tt = 7200  # training
    Tv = 540  # validation

    # load stimuli
    sf = load_file(stim_fname)
    st = load_stim_data(sf, 'st', Tt, fps, True, 'CORF' in stim_fname)
    print('ST LOADED')
    sv = load_stim_data(sf, 'sv', Tv, fps, True, 'CORF' in stim_fname)
    print('SV LOADED')
    sf.close()
    stimuli = {'train': st, 'validation': sv}
    print('STIMULI LOADED')

    # load responses
    responses = []
    response_sizes = []
    max_nan_len = []
    for fi, fname in enumerate(resp_fnames):
        f = load_file(fname)
        responses.append({'train': {}, 'validation': {}})
        response_sizes.append({})
        nan_lens = {'train': [], 'validation': []}
        for roi in rois_to_load:
            responses[fi]['train'][roi] = load_resp_data(f, 'rt', roi)
            responses[fi]['validation'][roi] = load_resp_data(f, 'rv', roi)
            nan_lens['train'].append(responses[fi]['train'][roi][0])
            nan_lens['validation'].append(responses[fi]['validation'][roi][0])
            response_sizes[fi][roi] = np.shape(responses[fi]['train'][roi][1])[1]  # same for validation
        f.close()
        max_nan_len.append({'train': np.max(nan_lens['train']), 'validation': np.max(nan_lens['validation'])})
    print('RESPONSES LOADED')

    return responses, stimuli, max_nan_len, response_sizes


if __name__ == '__main__':
    # constants
    Tr = 1
    fps = 15
    # Tt = 7200  # training
    # Tv = 540  # validation
    Tt = 100
    Tv = 100
    files_to_load = ['small_resp.mat']
    rois_to_load = ['v1rh', 'v2rh', 'v3rh', 'v4rh', 'v1lh', 'v2lh', 'v3lh', 'v4lh']

    # load responses
    responses = []
    response_sizes = []
    max_nan_len = []
    for fi, fname in enumerate(files_to_load):
        f = load_file(fname)
        responses.append({'train': {}, 'validation': {}})
        response_sizes.append({})
        nan_lens = {'train': [], 'validation': []}
        for roi in rois_to_load:
            responses[fi]['train'][roi] = load_resp_data(f, 'rt', roi)
            responses[fi]['validation'][roi] = load_resp_data(f, 'rv', roi)
            nan_lens['train'].append(responses[fi]['train'][roi][0])
            nan_lens['validation'].append(responses[fi]['validation'][roi][0])
            response_sizes[fi][roi] = np.shape(responses[fi]['train'][roi][1])[1]
        f.close()
        max_nan_len.append({'train': np.max(nan_lens['train']), 'validation': np.max(nan_lens['validation'])})
        # for rk, data in responses[fi].items():
        #     for roi in rois_to_load:
        #         responses[fi][rk][roi] = data[roi][max_nan_len[fi][rk]:]

    # load stimuli
    sf = load_file('small_stim.mat')
    st = load_stim_data(sf, 'st', Tt, fps, True, True)
    # sv = load_stim_data(sf, 'sv', Tv, fps, True, True)
    sf.close()

    # try it out
    fi = 0
    torv = 'train'
    run_len = 7
    batch_size = 2
    proba = rand_gen_data(responses[fi][torv], st, run_len, Tt, max_nan_len[fi][torv], batch_size, response_sizes[fi][torv])
    print(proba)
    print(np.shape(proba[0]), np.shape(proba[1]))
